package formallanguage;

/**
 * Klase ļauj pārbaudīt vai dotais vārds ir 
 * korekti izvedama teorēma manai formālajai teorijai.
 * 
 * L = {c, d, e}
 * Aksiomas: d, ec
 * Izveduma likumi: X / cXe, X / dXd
 * 
 * @author Matīss Zērvēns
 * @since 07.09.2014
 */
public class FormalLanguage 
{
    private static String acxioms [];
    private static Tree theoremTree = null;
    private static String lookupWord = "";
    private static int startingAcxiom = 0;
    
    private static String ruleOfInference1(String word)
    {
        String newWord = "c" + word + "e";        
        return newWord;
    }
    
    private static String ruleOfInference2(String word)
    {
        String newWord = "d" + word + "d";        
        return newWord;
    }
    
    private static void buildTree(Tree current, int length)
    {
        if(current == null)
        {
            current = new Tree();
            current.theorem = acxioms[startingAcxiom];
            theoremTree = current;
        }
    
        if(current.theorem.length() >= length)
        {
            return;
        }
        
        current.right = new Tree();
        current.right.theorem = ruleOfInference1(current.theorem);
        
        current.left = new Tree();
        current.left.theorem = ruleOfInference2(current.theorem);
        
        buildTree(current.right, length);
        buildTree(current.left, length);

    }
    
    private static boolean findWord(Tree current, boolean found)
    { 
        if(current.theorem.equals(lookupWord))
        {
            System.err.println(current.theorem);
            found = true;
        }

        if(found == false && current.right != null)
        {
            found = findWord(current.right, found);
        }
        
        if(found == false && current.left != null)
        {
            found = findWord(current.left, found);
        }

        return found;
    }
    
    private static boolean isCorrectProposition(String proposition)
    {
        boolean isCorrect = false;
        lookupWord = proposition;
        
        for(int i = 0; i < acxioms.length; i++)
        {
            startingAcxiom = i;
            buildTree(theoremTree, proposition.length());
            isCorrect = findWord(theoremTree, isCorrect);
            if(isCorrect) break;
            theoremTree = null;
        }
    
        return isCorrect;
    }
    
    private static void printTree(Tree node)
    {
        if(node == null) return;
        
        System.out.print(node.theorem + "  ");
        printTree(node.right);
        printTree(node.left);
    
    }

    public static void main(String[] args) 
    {
        acxioms = new String[2];
        acxioms[0] = "d";
        acxioms[1] = "ec";
        
        if(isCorrectProposition("cddcecedde"))
        {
            System.out.println("Correct proposition");
        }
        else
        {
            System.out.println("Incorrect proposition");
        
        }
        
        printTree(theoremTree);

    }
}
